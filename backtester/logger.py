def log(msg):
    print msg


def logError(msg):
    log('Error: ' + msg)


def logInfo(msg):
    log('Info: ' + msg)


def logWarn(msg):
    log('Warn: ' + msg)
